const env = process.env.NODE_ENV;

//configure .env when in development mode
if (env !== 'production') {
    require('dotenv').config();
}

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const indexRouter = require('./routes/index');

const app = express();

app.use(cors({
    origin: process.env.ENABLE_CORS_FOR.split(','),
    methods: 'GET, POST',
    allowedHeaders: 'Origin, Accept, X-Requested-With, Content-Type, Authorization',
    credentials: true,
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.use('/', indexRouter);


// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500).json({
        status: err.status || 500,
        message: err.message,
    });
});


module.exports = app;
