const Sequelize = require('sequelize');
const env = process.env.NODE_ENV;

let dbName = env === 'test' ? process.env.DB_NAME_TEST : process.env.DB_NAME;
let dbUserName = env === 'test' ? process.env.DB_USERNAME_TEST : process.env.DB_USERNAME;
let dbPassword = env === 'test' ? process.env.DB_PASSWORD_TEST : process.env.DB_PASSWORD;
let dbHost = env === 'test' ? process.env.DB_HOST_TEST : process.env.DB_HOST;

const config = {
    host: dbHost,
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    define: {
        timestamps: false
    }
}

if(env !== 'development'){
    config.logging = false;
}

const sequelize = new Sequelize(dbName, dbUserName, dbPassword,config);

module.exports = sequelize;