const express = require('express');
const router = express.Router();
const apiClient = require('../apiClient');
const db = require('../db');
const keywordsModel = require('../models/keywords')(db);
const Sequelize = require('sequelize');


//home page
router.get('/', (req, res, next) => {
  res.status(200).json({
    status: 200,
    message: "Welcome to jobs.search.gov API"
  });
});



/**
 * @api {get} /search Search Items
 * @apiName SearchItems
 * @apiGroup API
 *
 * @apiParam {String} q Search query.
 *
 * @apiSuccess (200) {String} status Status code.
 * @apiSuccess (200) {Array} data Search Result.
 * 
 * @apiError NothingFound There is no result 
 * @apiError InternalServerError Server error
 */
router.get('/search', async (req, res, next) => {

  if(!req.query.q){
    return res.status(200).json({
      status: 200,
      data: []
    });
  }

  try {
    
    const search = await apiClient.get('/jobs/search.json?query=' + req.query.q);

    return res.status(200).json({
      status: 200,
      data: search.data
    });

  } catch (e) {
    console.log(e.toString());
    return res.status(e.response ? e.response.status : 500).json({
      status: e.response ? e.response.status : 500,
      message: e.response ? e.response.data : "Internal Server Error",
      data: []
    });
  }


});


/**
 * @api {post} /save Save Keyword
 * @apiName SaveKeyword
 * @apiGroup DB
 *
 * @apiParam {String} keyword Search Keyword.
 *
 * @apiSuccess (201) {String} status Status code.
 * @apiSuccess (201) {String} message Response message.
 * @apiSuccess (201) {Integer} id Inserted row id.
 * 
 * @apiError BadRequest <keyword> parameter was not found in request body 
 * @apiError InternalServerError Server error
 * 
 */
router.post('/save', async (req, res, next) => {

  const requestBody = req.body;

  //check if <keyword> parameter was entered in request body
  if (!requestBody.keyword) {
    return res.status(400).json({
      status: 400,
      message: '400 Bad Request | <keyword> parameter is required',
    })
  }

  let keyword = requestBody.keyword;


  try {
    const save = await keywordsModel.create({ keyword: keyword.toLowerCase() });

    //check if insert id is exists otherwhise throw an error response
    if (!save.id) {
      return res.status(500).json({
        status: 500,
        message: "Internal server error",
        error: "Can't save data"
      });
    }

    return res.status(201).json({
      status: 201,
      message: 'Saved',
      id: save.id
    });

  } catch (e) {
    return res.status(500).json({
      status: 500,
      message: "Internal server error",
      error: e.toString()
    });
  }


});



/**
 * @api {get} /statistics Get statistics
 * @apiName GetStatistics
 * @apiGroup DB
 *
 * @apiSuccess (200) {String} status Status code.
 * @apiSuccess (200) {Array} data Inserted row id.
 * 
 * @apiError InternalServerError Server error
 * 
 */
router.get('/statistics', async (req, res, next) => {

  try {
    const data = await db.query(
      'SELECT keyword, COUNT(*) as total,DATE(created_at) AS searchDate FROM `keywords` GROUP BY keyword,searchDate',
      {
        type: Sequelize.QueryTypes.SELECT
      }
    );

    return res.status(200).json({
      status: 200,
      data: data
    });

  } catch (e) {
    return res.status(500).json({
      status: 500,
      message: "Internal server error",
      error: e.toString()
    });
  }

});



module.exports = router;
