const chai = require('chai');
const expext = chai.expect;
const chaiHttp = require('chai-http');
const should = chai.should();
const request = require('request');



chai.use(chaiHttp);

before(() => {
    const server = require('../app-test');
    const httpServer = require('http-shutdown')(server);
    global.server = httpServer;
})

after(() => {
    global.server.shutdown(() => {
        console.log("Server ShutDown");
    })
});


describe('Server', () => {
    
    it('Checking Server health', (done)=>{
        chai.request(global.server)
        .get('/')
        .end((err,res) => {
            //console.log(res)
            res.should.have.status(200);
            res.body.should.be.a('object')
            res.body.should.have.property('status')
            res.body.should.have.property('message')
            //res.should.be.json;
           // res.body.should.have.property('status')
            done()
        })
    })
})

describe('API Route', () => {

    it('/search/:query checking search router', (done)=>{
        chai.request(global.server)
        .get('/search/dev')
        .end((err,res) => {
            res.should.have.status(200);
            res.body.should.be.a('object')
            res.body.should.have.property('status')
            res.body.should.have.property('data')
            done()
        })
    })
    
    it('/search/:query checking search router if parameter not provided', (done)=>{
        chai.request(global.server)
        .get('/search')
        .end((err,res) => {
            res.should.have.status(404);
            res.body.should.be.a('object')
            res.body.should.have.property('status')
            res.body.should.have.property('message')
            done()
        })
    })


    it('/save checking keyword search', (done)=>{
        chai.request(global.server)
        .post('/save')
        .send({keyword:'test'})
        .end((err,res) => {
            res.should.have.status(201);
            res.body.should.be.a('object')
            res.body.should.have.property('status')
            res.body.should.have.property('message')
            res.body.should.have.property('id')
            done()
        })
    })

    it('/save checking if <keyword> parameter was not entered', (done)=>{
        chai.request(global.server)
        .post('/save')
        .send({})
        .end((err,res) => {
            res.should.have.status(400);
            res.body.should.be.a('object')
            res.body.should.have.property('status')
            res.body.should.have.property('message')
            done()
        })
    })

    it('/statistics checking statistic router', (done)=>{
        chai.request(global.server)
        .get('/statistics')
        .end((err,res) => {
            res.should.have.status(200);
            res.body.should.be.a('object')
            res.body.should.have.property('status')
            res.body.should.have.property('data')
            done()
        })
    })
})