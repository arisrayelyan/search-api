# Search API (search.gov)
- **[Demo](https://search-api.isra.io)**
- **[API Documentation](https://search-api-doc.isra.io)**

## Technologies & third party apis
- **Node.js**
- **Mysql**
- **As a third party API used [search.gov](https://search.gov/developer/jobs.html)**


## Requirements
- **NodeJS (Stable Version V > 8.9)**
- **Mysql (latest version)**

### Database manualy setup

Sequelize migration is not implemented yet so please import database manualy, you can find db structure and file [here](/db_files/search_api.sql)


### Development

Clone the repository and install dependencies

```
git clone https://bitbucket.org/arisrayelyan/search-api.git

cd /search-api

npm i
```

**Initiate envoirment file from dev.env template**


```
cp dev.env .env
```

after fill envoirment variables in .env file (database username, password etc.) 

**Start development server**


```
//unix users
npm run start:dev

//windows users
npm run start:dev:w
```

**Tests** 


```
//unix users
npm run test

//windows users
npm run test:w
```