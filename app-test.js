/**
 * Server setup for testing
 */

const app = require('./app');
const http = require('http');


const port = process.env.PORT || '3000';
app.set('port', port);

var server = http.createServer(app);

server.listen(port);


module.exports = server;